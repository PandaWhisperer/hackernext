import Link from 'next/link'
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

const Items = ({ items }) => {
  const classes = useStyles()

  return (
    <List className={classes.root}>
    { items.map(item => (
      <Link key={ item.id } href="/items/[id]" as={ `/items/${ item.id }` }>
        <ListItem button component="a">
          <ListItemText primary={ item.title } secondary={ item.pubDate }/>
        </ListItem>
      </Link>
    ))}
    </List>
  )
}

export default Items
