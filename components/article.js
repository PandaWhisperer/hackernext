import Paper from '@material-ui/core/Paper';

const Article = ({ article }) => (
  <Paper elevation={0}>
    { article ? (<>
      <h2>{ article.title }</h2>
      <a href={ article.link }>{ article.link }</a>
      <div>{ article.content }</div>
    </>) : (
      <p>Nothing selected.</p>
    )}
  </Paper>
)

export default Article
