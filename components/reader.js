import Grid from '@material-ui/core/Grid';
import Article from './article'
import Items from './items'

const Reader = ({ items, article }) => {
  return (
    <Grid container spacing={3}>
      <Grid item xs={4}>
        <Items items={items} />
      </Grid>
      <Grid item xs={8}>
        <Article article={article} />
      </Grid>
    </Grid>
  )
}

export default Reader
