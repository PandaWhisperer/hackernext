import Head from 'next/head'
import Reader from '../../components/reader'

const Home = ({ items, article }) => (
  <div>
    <Head>
      <title>Home</title>
    </Head>

    <Reader items={ items } article={ article } />
  </div>
)

import fetcher from '../_fetcher'

Home.getInitialProps = async({ req, query }) => {
  const fetchJSON = fetcher(req)
  const { id }  = query
  const items   = await fetchJSON('/api/items')
  const article = await fetchJSON(`/api/items/${id}`)
  console.log(`/api/items/${id}`, items, article)

  return { items, article }
}

export default Home
