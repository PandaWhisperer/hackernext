import fetch from 'isomorphic-unfetch'
import { parseStringPromise as parseXml } from 'xml2js'

export default async function get(req, res) {
  const result = await fetch('https://news.ycombinator.com/rss')
  const xml    = await result.text()

  if (result.ok) {
    const {rss} = await parseXml(xml, { explicitArray: false })
    const items = rss.channel.item.map(item => ({
      id: item.comments.split('=')[1], ...item
    }))

    res.status(200).json(items)
  } else {
    res.status(400).send(data)
  }
}
