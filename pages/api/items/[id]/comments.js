import fetch from 'isomorphic-unfetch'

async function fetchItem(id) {
  try {
    const result = await fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json`)
    return result.json()
  } catch {
    return null
  }
}

async function fetchRecursive(id, depth = 1) {
  const item = await fetchItem(id)

  if (item && item.kids && depth > 0) {
    item.kids = await Promise.all( item.kids.map(id => fetchRecursive(id, depth-1)) )
  }

  return item
}

export default async function get(req, res) {
  const { id, depth } = req.query
  const data = await fetchRecursive(id, depth)

  res.status(200).json(data)
}
