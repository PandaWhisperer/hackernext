import fetch from 'isomorphic-unfetch'
import { parseStringPromise as parseXml } from 'xml2js'
import scrape from 'ascrape'

export default async function get(req, res) {
  const { id } = req.query

  const result = await fetch('https://news.ycombinator.com/rss')
  const xml    = await result.text()

  if (result.ok) {
    const {rss} = await parseXml(xml, { explicitArray: false })
    const items = rss.channel.item.map(item => ({
      id: item.comments.split('=')[1], ...item
    }))
    const item = items.find(item => item.id === id)

    if (item) {
      scrape(item.link, (err, article, meta) => {
        res.status(200).json({
          ...item,
          title:   article.title,
          content: article.content.html(),
          description: article.excerpt
        })
      })
    } else {
      res.status(404).json({ message: 'Not found' })
    }
  } else {
    res.status(404).json({ message: 'Not found' })
  }
}
