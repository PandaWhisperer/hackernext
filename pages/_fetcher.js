import fetch from 'isomorphic-unfetch'

export default function makeFetcher(req) {
  const prefix = req ? `http://${req.headers['host']}` : ''

  return async (url) => {
    const response = await fetch(prefix + url)
    const data     = await response.json()

    return data
  }
}
