import Head from 'next/head'
import Reader from '../components/reader'

const Home = ({ items }) => (
  <div>
    <Head>
      <title>Home</title>
    </Head>

    <Reader items={ items } />
  </div>
)

import fetcher from './_fetcher'

Home.getInitialProps = async({ req }) => {
  const fetchJSON = fetcher(req)
  const items     = await fetchJSON('/api/items')

  return { items }
}

export default Home
