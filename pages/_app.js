import Head from 'next/head'
import { makeStyles } from '@material-ui/core/styles';
import Nav from '../components/nav'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  offset: theme.mixins.toolbar,
}));

const App = ({ Component, pageProps }) => {
  const classes = useStyles()

  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Nav />

      <div className={classes.offset} />

      <Component {...pageProps} />
    </>
  )
}

export default App
