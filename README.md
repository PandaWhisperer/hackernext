# HackerNext

HackerNews on Next.js - just a simple [Hackernews][1] interface (similar to [hn.premii.com][2]), built with Next.js. Just a little side project to teach myself React and Next.js.

[1]: https://news.ycombinator.com
[2]: http://hn.premii.com

## Installation

1. Clone the repo
2. Run `npm install`
3. Run `npm start` to start the app, or `npm run dev` to start the dev server

## To Do

- [x] Display a list of headlines
- [ ] Display article content
- [ ] Display article comments
